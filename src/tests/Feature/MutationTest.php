<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MutationTest extends TestCase
{
    /**
     * Comprobar la existencia del endpoint mutation
     * @return void
     */

    public function testHasRoute()
    {
        $response = $this->post('/mutation');

        $this->assertNotEquals(404,$response->status(),'La ruta no existe');
    }


    /**
     * Comprobar el comportamiento de la API
     * POST /mutation
     * @dataProvider provideADN
     * @return void
     */
    public function testPostMutation($adnA,$adnB)
    {
        // Caso A True

        $response = $this->post('/mutation',[
            'adn' => $adnA
        ]);

        $flagA = $response->status();

        // Caso B False
        $response = $this->post('/mutation',[
            'adn' => $adnB
        ]);

        $flagB = $response->status();

        $this->assertTrue($flagA == 200 && $flagB == 403,'El sistema no devolvio los status correctos');

    }

    /**
     * Comprobar el comportamiento del metodo hasMutation
     * @return void
     */
    // public function testHasMutation()
    // {
        
    // }

    /**
     * Comprobar el comportamiento del validador
     * @dataProvider privideOneBadFormed
     * @return void
     */
    public function testValidateMutation($adnA)
    {
        $response = $this->post('/mutation',[
            'adn' => $adnA
        ]);

        $flagA = $response->status();

        $response = $this->post('/mutation',[
            'adn' => $adnA
        ]);

        $flagB = $response->status();
        
        $response = $this->post('/mutation',[
            'adn' => $adnA
        ]);

        $flagC = $response->status();

        $this->assertTrue($flagA == 400 && $flagB == 400 && $flagC == 400);
    }

    /**
     * Comprobar la seguridad de la API
     * Ausencia del token jwt
     * @dataProvider provideADN
     * @return void
     */
    public function testJWTMutation($adnA,$adnB)
    {
        $response = $this->post('/mutation',[
            'adn' => $adnA
        ]);

        $this->assertEquals(403,$response->status());
    }

    

     /**
      * Crear matriz de estudio sin mutacion
      * @return array[]
      */
    public function provideADN()
    {
        // Con mutacion
        $matrizA = ['ATGCGA','CAGTGC','TTATTT','AGACGG','GCGTCA','TCACTG'];

        // Sin mutacion
        $matrizB = ['ATGCGA','CAGTGC','TTATGT','AGAAGG','CCCCTA','TCACTG'];

        return [$matrizA,$matrizB];
    }

    /**
     * Proveer con matrices que no cumplan con la validacion del endpoint
     * @return array[]
     */

     public function provideBadFormedAdn()
     {
        $matrizA = ['ATGCGA','CAGTGC','TTATTT','AGACGGC','GCGTC','TCACTG'];

        $matrizB = ['ATGCGA','CAGTGC','TTATTT','AGA','GCGTCA','TCACTG'];

        $matrizC = 'ATGCGACAGTGCTTATTTAGAGCGTCATCACTG';

        return [$matrizA,$matrizB,$matrizC];         
     }

    /**
     * Proveer de un unico genoma mal formado
     */
    public function privideOneBadFormed()
    {
        return [[['ATGCGA','CAGTGC','TTATTT','AGACGGC','GCGTC','TCACTG']]];
    }

}

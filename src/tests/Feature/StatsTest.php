<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\InvalidArgumentException;
use Tests\TestCase;

class StatsTest extends TestCase
{

    /**
     * Comprobar seguridad del endpoint stats
     * @return void
     */
    public function testJWTStats()
    {
        $response = $this->post('/stats');

        $this->assertEquals(403,$response->status());
    }

    /**
     * Comprobar el formato de salida del endpoint
     * @return void
     */
    public function testFormatStats()
    {
        $response = $this->post('/stats');

        $res = json_decode($response->content(),true);
        
        try {
            $this->assertArrayHasKey('count_mutations',$res);

            $this->assertArrayHasKey('count_no_mutation',$res);
            
            $this->assertArrayHasKey('ratio',$res);
        } catch (InvalidArgumentException $th) {
            // el sistema recibe una cadena vacia 

            $this->assertNotNull($res);
        }
        
    }

}

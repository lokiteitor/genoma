<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MutationController extends Controller
{
    /**
     * Recibe por POST un parametro dna con un arreglo de 
     * lineas de dna
     *  Devuelve HTTP 203 si es correcto y 400 si es incorrecto
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }
}
